package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(parkDatafilePath));
            String line = reader.readLine();

            Park park = null;
            ParkParsingException parkParsingException = new ParkParsingException("Exception while parsing park data.", null);
            while (line != null) {
                String[] splittedLine = line.split(":");
                if (splittedLine.length == 2) {
                    String title = splittedLine[0].substring(1, splittedLine[0].length() - 1);
                    String value = splittedLine[1].trim();
                    if (value.charAt(0) == '\"' && value.charAt(value.length() - 1) == '\'') {
                        value = value.substring(1, value.length() - 1);
                    }

                    Field nameTitleField = FieldName.class.getDeclaredField("value");
                    Object nameTitle = nameTitleField.get(FieldName.class);

                    if (title.equals(nameTitle)) {
// set legalName
                    } else if (title.equals("ownerOrganizationInn")) {
                        Field maxLengthField = MaxLength.class.getDeclaredField("value");
                        Object maxLength = maxLengthField.get(MaxLength.class);

                        if (!value.equals("null") && value.length() > 0) {
                            if (value.length() <= (int) maxLength) {
// set ownerOrganizationInn
                            } else {
                                List<ParkParsingException.ParkValidationError> validationErrors = parkParsingException.getValidationErrors();
                                validationErrors.add(new ParkParsingException.ParkValidationError(title, "Поле не может быть длиннее заданного значения."));
                                parkParsingException.setValidationErrors(validationErrors);
                            }
                        } else {
                            List<ParkParsingException.ParkValidationError> validationErrors = parkParsingException.getValidationErrors();
                            validationErrors.add(new ParkParsingException.ParkValidationError(title, "Поле не может быть пустым."));
                            parkParsingException.setValidationErrors(validationErrors);

                        }
                    } else if (title.equals("foundationYear")) {
                        if (!value.equals("null") && value.length() > 0) {
// set foundationYear
                        } else {
                            List<ParkParsingException.ParkValidationError> validationErrors = parkParsingException.getValidationErrors();
                            validationErrors.add(new ParkParsingException.ParkValidationError(title, "Поле не может быть пустым."));
                            parkParsingException.setValidationErrors(validationErrors); }
                    }
                }
                line = reader.readLine();
            }
            return park;
        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
